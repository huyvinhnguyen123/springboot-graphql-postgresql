package com.example.springgraphqldemo.mutation;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.springgraphqldemo.model.TaskModel;
import com.example.springgraphqldemo.repository.TaskRepository;

import graphql.kickstart.tools.GraphQLMutationResolver;

@Component
public class TaskMutation implements GraphQLMutationResolver {
	
	@Autowired
	private TaskRepository taskRepository;
	
	public TaskModel addTask(String title, Date date) {
		TaskModel taskModel = new TaskModel();
		taskModel.setTitle(title);
		taskModel.setDate(date);
		
		return taskRepository.saveAndFlush(taskModel);
	}
	
	public TaskModel updateTask(Integer id, String title, Date date) {
		TaskModel taskModel = new TaskModel();
		taskModel.setId(id);
		taskModel.setTitle(title);
		taskModel.setDate(date);
		
		return taskRepository.saveAndFlush(taskModel);
	}
	
	public Boolean deleteTask(Integer id) {
		taskRepository.deleteById(id);
		return true;
	}
	
}
