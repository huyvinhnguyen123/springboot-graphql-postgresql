package com.example.springgraphqldemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.springgraphqldemo.model.TaskModel;

public interface TaskRepository extends JpaRepository<TaskModel, Integer> {

}
