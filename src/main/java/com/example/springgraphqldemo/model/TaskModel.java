package com.example.springgraphqldemo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "m_task")
public class TaskModel implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "task_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_post")
	@SequenceGenerator(name = "seq_post", allocationSize = 5)
	private Integer id;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "CREATE_AT")
	private Date date;
	
	
	
//	public TaskModel(Integer id, String title, String info) {
//		this.id = id;
//		this.title = title;
//		this.info = info;
//	}
	
	

	public Date getDate() {
		return date;
	}



	public void setDate(Date date) {
		this.date = date;
	}



	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Override
	public String toString() {
		return "Task [ id= "+ id +", title= "+ title +"]";
	}
	
}
