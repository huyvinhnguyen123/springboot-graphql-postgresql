package com.example.springgraphqldemo.resolver;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.springgraphqldemo.model.TaskModel;
import com.example.springgraphqldemo.repository.TaskRepository;

import graphql.kickstart.tools.GraphQLQueryResolver;


@Component
public class TaskResolver implements GraphQLQueryResolver {
	
	@Autowired
	private TaskRepository taskRepository;
	
	public Iterable<TaskModel> allTasks(){ // dùng Iterable hay hơn dùng List
		return taskRepository.findAll();
	}	
	
//	public TaskModel taskModel(Integer id) {
//		return taskRepository.findById(id).orElseGet(null); 
//	}
	
	public Optional<TaskModel> taskById(Integer id){
		return taskRepository.findById(id);
	}
}
